package com.citi.training.products.dao;

import java.util.List;

import com.citi.training.products.model.Manufacturer;

public interface ManufacturerDao {
    List<Manufacturer> findAll();
    int create(Manufacturer manufacturer);
    Manufacturer findById(int id);
    void deleteById(int id);
}
