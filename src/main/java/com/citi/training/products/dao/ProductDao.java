package com.citi.training.products.dao;

import java.util.List;

import com.citi.training.products.model.Product;

public interface ProductDao {

    List<Product> findAll();
    int create(Product product);
    Product findById(int id);
    void deleteById(int id);
}
