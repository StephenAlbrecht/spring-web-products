package com.citi.training.products.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.products.dao.ManufacturerDao;
import com.citi.training.products.model.Manufacturer;

@Component
public class ManufacturerService {

    @Autowired
    ManufacturerDao manufacturerDao;

    public List<Manufacturer> findAll() {
        return manufacturerDao.findAll();
    }

    public int create(Manufacturer manufacturer) {
        return manufacturerDao.create(manufacturer);
    }

    public Manufacturer findById(int id) {
        return manufacturerDao.findById(id);
    }

    public void deleteById(int id) {
        manufacturerDao.deleteById(id);
    }
}
