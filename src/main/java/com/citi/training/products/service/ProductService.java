package com.citi.training.products.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.products.dao.ProductDao;
import com.citi.training.products.model.Product;

@Component
public class ProductService {

    @Autowired
    ProductDao productDao;

    public List<Product> findAll() {
        return productDao.findAll();
    }

    public int create(Product product) {
        return productDao.create(product);
    }

    public Product findById(int id) {
        return productDao.findById(id);
    }

    public void deleteById(int id) {
        productDao.deleteById(id);
    }
}
