package com.citi.training.products.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.products.exception.ProductNotFoundException;
import com.citi.training.products.model.Product;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlProductDaoTests {

    @SpyBean
    JdbcTemplate tpl;

    @Autowired
    MysqlProductDao mysqlProductDao;

    @Test
    public void test_createAndFindAll_works() {
        mysqlProductDao.create(new Product(1, "Beans", 99.99));
        assertThat(mysqlProductDao.findAll().size(), equalTo(1));
    }

    @Test
    public void test_createAndFindById_works() {
        int newId = mysqlProductDao.create(
                            new Product(1, "Jam", 12.99));
        assertNotNull(mysqlProductDao.findById(newId));
    }

    @Test(expected=ProductNotFoundException.class)
    public void test_createAndFindById_throwsNotFound() {
        mysqlProductDao.create(new Product(1, "Beans", 99.99));
        mysqlProductDao.findById(4);
    }
}
