package com.citi.training.products.model;

import org.junit.Test;

public class ManufacturerTests {

    @Test
    public void test_Manufacturer_fullConstructor() {
        Manufacturer testManufacturer = new Manufacturer(0, "Nike", "New York");
        assert(testManufacturer.getId() == 0);
        assert(testManufacturer.getName().equals("Nike"));
        assert(testManufacturer.getAddress().equals("New York"));
    }

    @Test
    public void test_Manufacturer_setters() {
        Manufacturer testManufacturer = new Manufacturer(0, "Nike", "New York");
        testManufacturer.setId(10);
        testManufacturer.setName("Adidas");
        testManufacturer.setAddress("Los Angeles");

        assert(testManufacturer.getId() == 10);
        assert(testManufacturer.getName().equals("Adidas"));
        assert(testManufacturer.getAddress().equals("Los Angeles"));
    }

    @Test
    public void test_Manufacturer_toString() {
        Manufacturer testManufacturer = new Manufacturer(0, "Nike", "New York");

        assert(testManufacturer.toString() != null);
        assert(testManufacturer.toString().contains(testManufacturer.getName()));
    }
}
