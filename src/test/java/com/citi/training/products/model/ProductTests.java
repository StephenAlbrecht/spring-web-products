package com.citi.training.products.model;

import org.junit.Test;

import com.citi.training.products.model.Product;

public class ProductTests {

    @Test
    public void test_Product_fullConstructor() {
        Product testProduct = new Product(0, "Jelly", 9.99);
        assert(testProduct.getId() == 0);
        assert(testProduct.getName().equals("Jelly"));
        assert(testProduct.getPrice() == 9.99);
    }

    @Test
    public void test_Product_setters() {
        Product testProduct = new Product(0, "Jelly", 9.99);
        testProduct.setId(10);
        testProduct.setName("Beans");
        testProduct.setPrice(20.00);

        assert(testProduct.getId() == 10);
        assert(testProduct.getName().equals("Beans"));
        assert(testProduct.getPrice() == 20.00);
    }

    @Test
    public void test_Product_toString() {
        Product testProduct = new Product(0, "Jelly", 9.99);

        assert(testProduct.toString() != null);
        assert(testProduct.toString().contains(testProduct.getName()));
    }
}
