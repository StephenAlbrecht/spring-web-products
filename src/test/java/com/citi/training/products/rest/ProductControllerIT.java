package com.citi.training.products.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.products.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
public class ProductControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;
 
    @Test
    public void findAll_returnsList() {
        restTemplate.postForEntity(ProductController.BASE_PATH,
                                   new Product(-1, "Car", 2.2), Product.class);
        ResponseEntity<List<Product>> findAllResponse = restTemplate.exchange(
                                ProductController.BASE_PATH,
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Product>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
        assertTrue(findAllResponse.getBody().get(0).getName().equals("Car"));
    }
 
}
