package com.citi.training.products.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.products.dao.ManufacturerDao;
import com.citi.training.products.model.Manufacturer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ManufacturerServiceTests {

    @Autowired
    ManufacturerService manufacturerService;

    @MockBean
    private ManufacturerDao mockManufacturerDao;

    @Test
    public void test_findAllRuns() {
        List<Manufacturer> testList = new ArrayList<>();
        testList.add(new Manufacturer(1, "Nike", "New York"));

        when(mockManufacturerDao.findAll()).thenReturn(testList);
        List<Manufacturer> returnedList = manufacturerService.findAll();

        verify(mockManufacturerDao).findAll();
        assertEquals(testList, returnedList);
    }

    @Test
    public void test_findByIdRuns() {
        int id = 1;
        Manufacturer testManufacturer = new Manufacturer(id, "Nike", "New York");

        when(mockManufacturerDao.findById(id)).thenReturn(testManufacturer);
        Manufacturer returnedManufacturer = manufacturerService.findById(id);

        verify(mockManufacturerDao).findById(id);
        assertEquals(testManufacturer, returnedManufacturer);
    }

    @Test
    public void test_createRuns() {
        int testId = 1;
        Manufacturer manufacturer = new Manufacturer(99, "Nike", "New York");

        when(mockManufacturerDao.create(any(Manufacturer.class))).thenReturn(testId);
        int returnedId = manufacturerService.create(manufacturer);

        verify(mockManufacturerDao).create(manufacturer);
        assertEquals(testId, returnedId);
    }

    @Test
    public void test_deleteRuns() {
        int id = 1;

        manufacturerService.deleteById(id);

        verify(mockManufacturerDao).deleteById(id);
    }
}
