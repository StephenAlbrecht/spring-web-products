package com.citi.training.products.service;


import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.products.dao.ProductDao;
import com.citi.training.products.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {

    @Autowired
    ProductService productService;

    @MockBean
    private ProductDao mockProductDao;

    @Test
    public void test_findAllRuns() {
        List<Product> testList = new ArrayList<>();
        testList.add(new Product(1, "Ham", 9.99));

        when(mockProductDao.findAll()).thenReturn(testList);
        List<Product> returnedList = productService.findAll();

        verify(mockProductDao).findAll();
        assertEquals(testList, returnedList);
    }

    @Test
    public void test_findByIdRuns() {
        int id = 1;
        Product testProduct = new Product(id, "Ham", 9.99);

        when(mockProductDao.findById(id)).thenReturn(testProduct);
        Product returnedProduct = productService.findById(id);

        verify(mockProductDao).findById(id);
        assertEquals(testProduct, returnedProduct);
    }

    @Test
    public void test_createRuns() {
        int testId = 1;
        Product product = new Product(99, "Ham", 9.99);

        when(mockProductDao.create(any(Product.class))).thenReturn(testId);
        int returnedId = productService.create(product);

        verify(mockProductDao).create(product);
        assertEquals(testId, returnedId);
    }

    @Test
    public void test_deleteRuns() {
        int id = 1;

        productService.deleteById(id);

        verify(mockProductDao).deleteById(id);
    }
}
